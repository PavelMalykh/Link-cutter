Hi there!

**Project description:**

 - The link generator resource '/generate' can handle POST requests, which contains original link (as a request body) and returns the 'short' link.  
Example: POST /generate with body: `{ “oginrial”: “https://ya.ru” }` response is: `{ “link”: “api/l/{short-name}” }`

 - Redirect resource `'/api/l/{short-name}'` can redirect to the original link.  
Example: the original link: `“https://ya.ru”` and it's short link: `'api/l/TolDC'`, so trying to open `"http://localhost:8080/api/l/TolDC"` will redirect to `“https://ya.ru”`

 - Statistic for short links is here: `/stats/{short-name}` can handle GET-request and returns statistic of clicks on a specific link. There are several parametres which should be returned in the response body:  
*link* - 'short link'.  
*original* - 'original link'.  
*rank* - 'place of the link in the top queries'.  
*count* - 'number of requests for a short link'.  
Example: GET `'/stats/some-short-name'` response is: `{ “link”: “api/l/short-name”, “original”: “https://ya.ru” “rank”: 1, “count”: 4356 }` 

 - Link rating resource `'/stats'` should process a GET request and returns statistics of requests sorted by frequency of requests in descending order and the ability to page-by-page.  
There are several optional parametres, which can be passed:  
*page* - 'page number'.  
*count* - 'number of records displayed per page, maximum possible value 100 (inclusive)'.  
As a response it should be an array of JSON objects  
Request example: `GET /stats?page=1&count=2`.  
Response example:  
```
[ 
    { “link”: “api/l/short-name”, “original”: “http://ya.ru” “rank”: 1, “count”: 100 }, 
    { “link”: “api/l/some-another-short-name”, “original”: “http://another-server.com/some/url” “rank”: 2, “count”: 10 } 
]
```


**How you can build a project:**

 - First of all you need to download and build docker container with psql (follow the link) https://gitlab.com/PavelMalykh/synthesis-at/-/blob/master/src/main/resources/database/docker-compose.yml. After downloading this file, you need 'cd' to this folder and just run 'docker-compose up'. Also to be sure that container is running, you can run 'docker-compose ps' and check status (it should be 'Up')

- Then you need to 'cd' to the folder, where you cloned this repo and run a command 'mvn spring-boot:run'

**Available methods:**
```
 - POST /generate
 - GET /stats?page=1&count=2 (page and count are not required parametres)
 - GET /stats/some-short-name 
 - GET /api/l/short-name (redirect to the original link)
```


 **Killer feature**:
 After running and passing all tests, just write 'allure serve' via command line (from the main directory) and you can see test results.