package com.example.linkCutter.components;

import org.springframework.web.servlet.view.RedirectView;

public class Redirector {

    public static RedirectView redirect(String url) {
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl(url);
        return redirectView;
    }
}