package com.example.linkCutter.components;

import com.example.linkCutter.exception.BadRequestException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;

public class LinkCutter {

    public static String cutter(String originalString) {
        // regexp to find all matches in url, so if we pass 'https://some.com/tert/url?param=1'
        // we will have 2 groups [0] = https://some.com/tert/url?param=1 , [1] = some.com/tert/url?param=1
        // need to replace only the second part
        String regexp = "^https?\\:\\/\\/(.+)";
        Pattern pattern = Pattern.compile(regexp);
        String result = originalString;
        Matcher matcher = pattern.matcher(result);


        if (!matcher.find()) {
            throw new BadRequestException();
        }

        String replacementPart = "api/l/" + randomAlphanumeric(5);
        return result.replace(matcher.group(1), replacementPart);
    }
}