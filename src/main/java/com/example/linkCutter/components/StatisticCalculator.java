package com.example.linkCutter.components;

import com.example.linkCutter.domain.Link;
import com.example.linkCutter.repository.LinksRepo;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StatisticCalculator {
    private final LinksRepo linksRepo;

    @Autowired
    public StatisticCalculator(LinksRepo linksRepo) {
        this.linksRepo = linksRepo;
    }

    public void countRanks(List<Link> listOfLinks) {
        listOfLinks.sort((Comparator.comparingInt(Link::getCount)).reversed());
        for (Link link : listOfLinks) {
            link.setRank(listOfLinks.indexOf(link) + 1);
        }
        linksRepo.saveAll(listOfLinks);
    }


    /*
     * Split a list to lists according to count parameter
     * for example: we have 10 objects and a 'count' parameter (=4),
     * so we split them into 3 groups, the first one has 4 items, the second one has the same amount (4), the last one has 2
     */
    public static List<List<Link>> splitListByCount(List<Link> linksList, int count) {
        return Lists.partition(linksList, count);
    }

    // if page > collection size, it means that we need to return an empty list
    public static List<Link> getLinksPage(int pageInt, List<List<Link>> listOfLinkPartitionByPages) {
        if (pageInt >= listOfLinkPartitionByPages.size()) {
            return Collections.emptyList();
        }
        return listOfLinkPartitionByPages.get(pageInt);
    }

    public static void checkForNegative(int valueInt) {
        if (valueInt <= 0) throw new AssertionError("Value should be greater than 0");
    }

    public static int tryParse(String value) {
        int valueInt;
        try {
            valueInt = Integer.parseInt(value);
        } catch (NumberFormatException ex) {
            throw new NumberFormatException("Value should be an integer");
        }

        checkForNegative(valueInt);
        return valueInt;
    }
}