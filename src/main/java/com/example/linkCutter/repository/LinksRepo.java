package com.example.linkCutter.repository;

import com.example.linkCutter.domain.Link;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LinksRepo extends JpaRepository<Link, Long> {

    Optional<Link> findByLinkContaining(String link);

    Optional<Link> findByOriginal(String originalLink);
}