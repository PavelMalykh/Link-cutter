package com.example.linkCutter.controller;


import com.example.linkCutter.components.LinkCutter;
import com.example.linkCutter.domain.Link;
import com.example.linkCutter.repository.LinksRepo;
import com.example.linkCutter.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class LinkGenerator {
    private final LinksRepo linksRepo;

    @Autowired
    public LinkGenerator(LinksRepo linksRepo) {
        this.linksRepo = linksRepo;
    }

    @PostMapping("/generate")
    @JsonView(View.link.class)
    public Link createLink(@RequestBody Link link) {
        if (doesLinkAlreadyExist(link)) return linksRepo.findByOriginal(link.getOriginal()).get();

        String shortLink = LinkCutter.cutter(link.getOriginal());
        link.setLink(shortLink);
        linksRepo.save(link);

        return link;
    }

    private boolean doesLinkAlreadyExist(Link link) {
        return linksRepo.findByOriginal(link.getOriginal()).isPresent();
    }
}