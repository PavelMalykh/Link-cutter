package com.example.linkCutter.controller;

import com.example.linkCutter.components.StatisticCalculator;
import com.example.linkCutter.domain.Link;
import com.example.linkCutter.exception.NotFoundException;
import com.example.linkCutter.repository.LinksRepo;
import com.example.linkCutter.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.linkCutter.components.StatisticCalculator.getLinksPage;
import static com.example.linkCutter.components.StatisticCalculator.splitListByCount;
import static com.google.common.primitives.Ints.tryParse;

@RestController
@RequestMapping("/stats")
public class Statistic {
    private final LinksRepo linksRepo;

    @Autowired
    public Statistic(LinksRepo linksRepo) {
        this.linksRepo = linksRepo;
    }

    @GetMapping
    @JsonView(View.allFields.class)
    public List<Link> getAllStat(@RequestParam(required = false) String page, @RequestParam(required = false) String count) {

        List<Link> linksList = linksRepo.findAll();
        linksList.sort((Comparator.comparingInt(Link::getCount)).reversed());
        StatisticCalculator statisticCalculator = new StatisticCalculator(linksRepo);

        // if no count and page parameters
        if (count == null && page == null) {
            return linksList;
        // if only page is presented
        } else if (page != null && count == null) {

            int pageInt = tryParse(page);
            int defaultCount = 100;
            List<List<Link>> listOfLinkPartitionByPages = splitListByCount(linksList, defaultCount);
            return getLinksPage(pageInt - 1, listOfLinkPartitionByPages);
        // if only count is presented
        } else if (page == null) {

            int countInt = tryParse(count);
            countInt = countInt > 10 ? Math.min(linksList.size(), 10) : countInt;

            return IntStream.range(0, countInt).mapToObj(linksList::get).collect(Collectors.toList());
        }

        // if both of parameters are presented
        int pageInt = tryParse(page);
        int countInt = tryParse(count);

        List<List<Link>> listOfLinkPartitionByPages = splitListByCount(linksList, countInt);

        return getLinksPage(pageInt - 1, listOfLinkPartitionByPages);

    }

    @GetMapping("/{short-name}")
    @JsonView(View.allFields.class)
    public Link getLinkStats(@PathVariable("short-name") String shortName) {
        return linksRepo.findByLinkContaining(shortName).orElseThrow(NotFoundException::new);
    }
}