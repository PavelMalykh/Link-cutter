package com.example.linkCutter.controller;

import com.example.linkCutter.components.StatisticCalculator;
import com.example.linkCutter.domain.Link;
import com.example.linkCutter.exception.NotFoundException;
import com.example.linkCutter.repository.LinksRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import java.util.List;

import static com.example.linkCutter.components.Redirector.redirect;

@RestController
@RequestMapping
public class LinkRedirector {
    private final LinksRepo linksRepo;

    @Autowired
    public LinkRedirector(LinksRepo linksRepo) {
        this.linksRepo = linksRepo;
    }

    @GetMapping("/api/l/{short-name}")
    public RedirectView getLink(@PathVariable("short-name") String shortName) {
        Link link = linksRepo.findByLinkContaining(shortName).orElseThrow(NotFoundException::new);
        int updatedCount = link.getCount() + 1;
        String url = link.getOriginal();
        link.setCount(updatedCount);
        linksRepo.save(link);

        List<Link> linksList = linksRepo.findAll();
        new StatisticCalculator(linksRepo).countRanks(linksList);

        return redirect(url);
    }
}