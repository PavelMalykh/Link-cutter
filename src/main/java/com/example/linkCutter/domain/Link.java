package com.example.linkCutter.domain;

import com.example.linkCutter.view.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import javax.persistence.Id;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Link {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    @JsonView(View.link.class)
    private String link;
    @JsonView(View.allFields.class)
    private String original;
    @JsonView(View.allFields.class)
    private int rank = 0;
    @JsonView(View.allFields.class)
    private int count = 0;
}