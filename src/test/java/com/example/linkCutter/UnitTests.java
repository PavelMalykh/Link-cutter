package com.example.linkCutter;

import com.example.linkCutter.domain.Link;
import com.example.linkCutter.exception.BadRequestException;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.example.linkCutter.components.LinkCutter.cutter;
import static com.example.linkCutter.components.StatisticCalculator.checkForNegative;
import static com.example.linkCutter.components.StatisticCalculator.getLinksPage;
import static com.example.linkCutter.components.StatisticCalculator.splitListByCount;
import static com.example.linkCutter.components.StatisticCalculator.tryParse;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UnitTests {

    @Test
    void checkCorrectHttpLinkLength() {
        int expectedHTTPLength = 18;
        int actualHTTPLength = cutter("http://google.com").length();

        assertEquals(expectedHTTPLength, actualHTTPLength);
    }

    @Test
    void checkCorrectHttpsLinkLength() {
        int expectedHTTPSLength = 19;
        int actualHTTPSLength = cutter("https://google.com").length();

        assertEquals(expectedHTTPSLength, actualHTTPSLength);
    }

    @Test
    void checkCroppedLinkContainsHttpPart() {
        String expected = "http://api/l/";
        String actual = cutter("http://google.com");

        assertTrue(actual.contains(expected));
    }

    @Test
    void checkCroppedLinkContainsHttpsPart() {
        String expected = "https://api/l/";
        String actual = cutter("https://google.com");

        assertTrue(actual.contains(expected));
    }

    @Test
    void checkIncorrectLinkHttpsss() {
        assertThrows(BadRequestException.class, () -> {
            cutter("httpsss://google.com");
        });
    }

    @Test
    void checkIncorrectLink() {
        assertThrows(BadRequestException.class, () -> {
            cutter("https//google.com");
        });
    }

    @Test
    void checkTryParseIncorrectFormat() {
        assertThrows(NumberFormatException.class, () -> {
            tryParse("value");
        });
    }

    @Test
    void checkTryParseCorrectNumber() {
        int actual = tryParse("12");
        int expected = 12;

        assertEquals(expected, actual);
    }

    @Test
    void checkTryParseDoubleValue() {
        assertThrows(NumberFormatException.class, () -> {
            tryParse("12.4");
        });
    }

    @Test
    void checkForNegativeZero() {
        assertThrows(AssertionError.class, () -> {
            checkForNegative(0);
        });
    }

    @Test
    void checkForNegativeNegativeNumber() {
        assertThrows(AssertionError.class, () -> {
            checkForNegative(-1);
        });
    }

    @Test
    void checkForNegativePositiveNumber() {
        assertDoesNotThrow(() -> checkForNegative(1));
    }

    @Test
    void checkCorrectSplitByCount() {
        int actualSize = splitListByCount(Arrays.asList(new Link(), new Link(), new Link()), 2).size();
        int expectedSize = 2;

        assertEquals(expectedSize, actualSize);
    }

    @Test
    void checkGetLinksPageEmptyList() {
        List<Link> actualList = getLinksPage(4, Arrays.asList(Arrays.asList(new Link(), new Link(), new Link())));
        List<Link> expectedList = Collections.emptyList();

        assertEquals(expectedList, actualList);
    }

    @Test
    void checkGetLinksPageNeededPage() {
        List<Link> actualList = getLinksPage(2, Arrays.asList(Arrays.asList(new Link()), Arrays.asList(new Link()), Arrays.asList(new Link())));
        List<Link> expectedList = Arrays.asList(new Link());

        assertEquals(expectedList, actualList);
    }
}